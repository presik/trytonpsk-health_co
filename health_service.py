# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import date
from decimal import Decimal

from trytond.model import fields, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.pyson import Eval
from trytond.exceptions import UserError
from trytond.i18n import gettext

ZERO = Decimal('0')


class HealthService(metaclass=PoolMeta):
    __name__ = 'gnuhealth.health_service'
    copayment = fields.Numeric('Copayment', digits=(16, 2),
        depends=['service_line'], states={'readonly': True})
    # moderator_fee = fields.Numeric('Moderator Fee', digits=(16, 2),
    #     depends=['service_line'], states={'readonly': True})
    # copayment_voucher = fields.Many2One('account.voucher',
    #    'Copayment Voucher',
    #     states={'readonly': True})
    # moderator_fee_voucher = fields.Many2One('account.voucher',
    #    'Moderator Fee Voucher',
    #     states={'readonly': True})

    @classmethod
    def __setup__(cls):
        super(HealthService, cls).__setup__()
        cls._buttons.update({
            'pay_copayment': {
                'invisible': Eval('copayment_voucher'),
            },
            'pay_moderator': {
                'invisible': Eval('moderator_fee_voucher'),
            },
        })

    @fields.depends('patient', 'invoice_to')
    def on_change_patient(self, name=None):
        if self.patient and self.patient.name.eps:
            self.invoice_to = self.patient.name.eps.id

    @staticmethod
    def default_service_date():
        return date.today()

    @fields.depends('service_line', 'copayment', 'moderator_fee')
    def on_change_service_line(self):
        sale_price_list = None
        self.copayment = ZERO
        self.moderator_fee = ZERO
        if self.patient and self.patient.name.sale_price_list:
            sale_price_list = self.patient.name.sale_price_list

        if not sale_price_list:
            return
        for line in self.service_line:
            if line.product and line.product.template.type_payment == 'copayment':
                copayment = float(line.product.template.unit_price) * sale_price_list.copayment_rate / 100
                self.copayment = Decimal(round(copayment, 2))
                continue
            elif line.product and line.product.template.type_payment == 'moderator_fee':
                self.moderator_fee = sale_price_list.moderator_fee

    @classmethod
    @ModelView.button
    def pay_copayment(cls, services):
        for service in services:
            if not service.copayment:
                continue
            service.create_voucher('copayment', service.copayment)

    @classmethod
    @ModelView.button
    def pay_moderator(cls, services):
        for service in services:
            if not service.moderator_fee:
                continue
            service.create_voucher('moderator_fee', service.moderator_fee)

    def create_voucher(self, kind, amount):
        pool = Pool()
        Voucher = pool.get('account.voucher')
        Config = pool.get('account.voucher_configuration')
        HealthConfig = pool.get('health.sequences')
        PayMode = pool.get('account.voucher.paymode')
        health_config = HealthConfig(1)
        config = Config.get_configuration()
        today = date.today()
        payment_mode = None

        if config.default_payment_mode:
            payment_mode = config.default_payment_mode
        else:
            values = PayMode.search([
                ('company', '=', Transaction().context.get('company'))
            ])
            if values:
                payment_mode = values[0]

        if not payment_mode:
            raise UserError(gettext('health_co.msg_missing_paymode'))

        if not health_config.account_copayment:
            raise UserError(gettext('health_co.msg_missing_account_copayment'))
        if kind == 'copayment':
            account = health_config.account_copayment
            field_voucher = 'copayment_voucher'
        else:
            account = health_config.account_moderator_fee
            field_voucher = 'moderator_fee_voucher'

        voucher_type = 'receipt'
        party = self.patient.name

        account_id = Voucher.get_account(voucher_type, payment_mode)

        voucher_to_create = {
            'party': party.id,
            'voucher_type': voucher_type,
            'date': today,
            'description': self.name,
            'payment_mode': payment_mode.id,
            'state': 'draft',
            'account': account_id,
            'journal': payment_mode.journal.id,
            'lines': [('create', [])],
            'method_counterpart': 'one_line',
        }

        detail = ('Health Service ' + self.name)
        voucher_to_create['lines'][0][1].append({
            'detail': detail,
            'amount': amount,
            'party': self.invoice_to.id,
            'amount_original': amount,
            'account': account.id,
        })

        voucher, = Voucher.create([voucher_to_create])
        voucher.on_change_lines()
        voucher.save()
        Voucher.process([voucher])
        self.write([self], {field_voucher: voucher.id})
        self.save()


class HealthServiceLine(metaclass=PoolMeta):
    __name__ = 'gnuhealth.health_service.line'
    cups = fields.Many2One('gnuhealth.procedure', 'CUPS')


"""
class CreateServiceInvoice:
    'Create Service Invoice'
    __name__ = 'health.service.invoice.create'

    @classmethod
    def __setup__(cls):
        super(CreateServiceInvoice, cls).__setup__()
        cls._error_messages.update({
            'patient_without_insurance': 'Patient without insurance!',
            })

    def transition_create_service_invoice(self):
        pool = Pool()
        HealthService = pool.get('health.health_service')
        Invoice = pool.get('account.invoice')
        Party = pool.get('party.party')
        Journal = pool.get('account.journal')

        services = HealthService.browse(Transaction().context.get(
            'active_ids'))
        invoices = []

        #Invoice Header
        for service in services:
            if service.invoice_to:
                party = service.invoice_to
            else:
                party = service.patient
            if service.state == 'invoiced':
                    self.raise_user_error('duplicate_invoice')
            invoice_data = {}
            invoice_data['description'] = service.desc
            invoice_data['party'] = party.id
            invoice_data['health_service'] = service.id
            invoice_data['patient'] = service.patient.id
            invoice_data['type'] = 'out_invoice'
            invoice_data['account'] = party.account_receivable.id
            invoice_data['invoice_date'] = service.service_date

            journals = Journal.search([
                ('type', '=', 'revenue'),
                ], limit=1)

            if journals:
                journal, = journals
            else:
                journal = None

            invoice_data['journal'] = journal.id

            party_address = Party.address_get(party, type='invoice')
            if not party_address:
                self.raise_user_error('no_invoice_address')
            invoice_data['invoice_address'] = party_address.id
            invoice_data['reference'] = service.name


            if not party.customer_payment_term:
                self.raise_user_error('no_payment_term')

            invoice_data['payment_term'] = party.customer_payment_term.id

            #Invoice Lines
            seq = 0
            invoice_lines = []

            #FIXME: Add price list based on context of the Party
            for line in service.service_line:
                seq = seq + 1
                account = line['product'].template.account_revenue_used.id
                if line['to_invoice']:
                    invoice_lines.append(('create', [{
                            'product': line.product.id,
                            'description': '[' + line.product.code + '] ' + line.desc,
                            'quantity': line.qty,
                            'account': account,
                            'unit': line.product.default_uom.id,
                            'unit_price': line.product.list_price,
                            'sequence': seq,
                            'service_start': line.from_date,
                            'service_end': line.to_date,
                        }]))
                invoice_data['lines'] = invoice_lines

            invoices.append(invoice_data)

        Invoice.create(invoices)

        # Change to invoiced the status on the service document.
        HealthService.write(services, {'state': 'invoiced'})

        return 'end'
"""
